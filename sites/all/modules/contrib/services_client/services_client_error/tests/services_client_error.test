<?php

require_once drupal_get_path('module', 'services_client') . '/tests/services_client.test';

/**
 * Test error handling code.
 */
class ServicesClientErrorWebTestCase extends ServicesClientBaseWebTestCase {

  protected $admin;
  protected $connection;

  public static function getInfo() {
    return array(
      'name' => 'Errors handling',
      'description' => 'Tests services client error handling',
      'group' => 'Services Client',
    );
  }

  function setUp() {
    parent::setUp('services_client_error');
  }

  protected function loadError($id) {
    return services_client_error_load($id);
  }

  public function testServicesClientErrors() {
    // Login admin user.
    $this->drupalLogin($this->admin);

    // Load errors page
    $this->drupalGet('admin/structure/services_client/errors');
    $this->assertText('No syncing errors. Wohoo!', 'By default no errors are present');

    $content_type = $this->drupalCreateContentType(array(
      'type' => 'post',
      'name' => 'Post',
    ));
    $node = $this->drupalCreateNode(array('type' => 'post', 'title' => 'Test post'));
    $result = services_client_process_events('save', $node, 'node');

    $event = $this->createSCEvent();

    // Add new property condition.
    $conditions['status'] = $this->addEventCondition($event, 'ServicesClientPropertyCondition', array(
      'property' => 'status',
      'condition' => 'equals',
      'value' => '1',
    ));
    $conditions['status'] = $this->addEventCondition($event, 'ServicesClientPropertyCondition', array(
      'property' => 'type',
      'condition' => 'equals',
      'value' => 'post',
    ));

    // Add uid mapping.
    $mapping['uid'] = $this->addEventMapping($event, 'ServicesClientPropertyReader', array('property' => 'uid'), 'ServicesClientPropertyFormatter', array('property' => 'uid'));
    $reader = array('field' => 'body', 'property' => 'value');
    $formatter = array('field' => 'field_body', 'property' => 'value');
    $mapping['body'] = $this->addEventMapping($event, 'ServicesClientFieldReader', $reader, 'ServicesClientFieldFormatter', $formatter);
    $formatter = array('field' => 'field_body_d6', 'property' => 'value');
    $mapping['body_d6'] = $this->addEventMapping($event, 'ServicesClientFieldReader', $reader, 'ServicesClientFieldD6Formatter', $formatter);
    $this->saveEventConfiguration($event, array(), TRUE);
    $this->enabledEvent($event);

    $node->_services_client['origin'] = 'remote_site';
    $node->_services_client['visted'] = array('remote_site');
    $handler = $event->getHandler();
    $result = $handler->setEntity($node)->execute();
    module_invoke_all('services_client_process_events', array($result));
    $this->assertIdentical($result->success(), FALSE, "Looping event is reporting correct result status.");
    $this->assertIdentical($result->error_type, ServicesClientErrorType::LOOP, "Loop error type is reported.");
    $this->drupalGet('admin/structure/services_client/errors');
    $this->assertText('No syncing errors. Wohoo!', "Loop error haven't triggered any error.");

    unset($node->_services_client);
    $result = services_client_process_events('save', $node, 'node');
    $result = reset($result);

    $this->drupalGet('admin/structure/services_client/errors');
    $this->assertNoText('No syncing errors. Wohoo!', 'Error is captured on errors page.');

    // Load latest error.
    $error = $this->loadError(1);
    $this->assertIdentical($error['entity']->nid, $node->nid, 'Stored error entity matched failed entity.');
    $this->assertEqual($error['error_code'], $result->error_code, "Error code is saved correctly.");
    $this->assertIdentical($error['error_message'], $result->error_message, "Error message is saved correctly.");

    $result = services_client_process_events('save', $node, 'node');
    $result = reset($result);
    $original_error = $this->loadError(1);
    $this->assertEqual($original_error['status'], SC_ERROR_OVERWRITTEN, "Old error got overwritten when new event is created.");
    $this->drupalGet('admin/structure/services_client/errors/1/log');
    $this->assertText(services_client_error_status_title(SC_ERROR_OVERWRITTEN));

    // Load new error. Test retries.
    $error = $this->loadError(2);
    $result = services_client_error_retry($error);
    // Refresh error array
    $error = $this->loadError(2);
    $this->assertEqual($error['retries'], 1, "Re-try was recorded.");
    $this->assertIdentical($result->sc_error_retries, 1, "Number or retries was added to result object.");

    // Test deleting error
    $this->drupalPost('admin/structure/services_client/errors/1/delete', array(), "Confirm");
    $this->assertText('Error was deleted.');
    $error = $this->loadError(1);
    $this->assertTrue(empty($error), 'Error was deleted from db.');
    $result = db_query("SELECT * FROM {services_client_error_log} WHERE eid = :eid", array(':eid' => 1))->fetchAll();
    $this->assertTrue(empty($result), "All error log messages were removed.");
  }
}
