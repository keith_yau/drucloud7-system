<?php

/**
 * @file
 * Installation file for services_client module.
 */

/**
 * Implements hook_requirements().
 */
function services_client_requirements($phase) {
  $requirements = array();
  $t = get_t();
  // In case oauth_common is installed, skip the check.
  // Because oauth_common does this check already.
  if (!module_exists('oauth_common')) {
    $curl_available = function_exists('curl_init');

    $requirements['services_client_curl'] = array(
      'title' => $t('Services client'),
      'value' => $curl_available ? $t('cURL library Enabled') : $t('cURL library not found'),
    );
    if (!$curl_available) {
      $requirements['services_client_curl'] += array(
        'severity' => REQUIREMENT_ERROR,
        'description' => $t("ServicesClientConnectionCurlRequest requires the PHP <a href='!curl_url'>cURL</a> library.",
          array('!curl_url' => 'http://php.net/manual/en/curl.setup.php')),
      );
    }
  }
  return $requirements;
}

/**
 * Implementation of hook_schema().
 */
function services_client_schema() {
  $schema = array();

  $schema['services_client_connection_event'] = array(
    'description' => 'Connection events',
    'export' => array(
      // Unique key of the connection hooks
      'key' => 'name',
      'admin_title' => 'title',
      // Description of key.
      'key name' => 'Primary ID Field for connection event',
      // For now, we don't allow to disable this.
      'can disable' => TRUE,
      // Variable name to use in exported code.
      'identifier' => 'connection_event',
      // Name of class that should be initialized
      'object' => 'ServicesClientEvent',
      // Use the same hook as the API name below.
      'default hook' => 'default_services_client_connection_event',
      // CTools API implementation.
      'api' => array(
        'owner' => 'services_client',
        'api' => 'services_client_connection_event',
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'eid' => array(
        'type'        => 'serial',
        'description' => 'Primary ID field for the table. Not used for anything except internal lookups.',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
        'no export'   => TRUE,
      ),
      'connection' => array(
        'description' => 'The name of the connection.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
      ),
      'name' => array(
        'description' => 'The name of the hook.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
      ),
      'title' => array(
        'description' => 'The title of the hook.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
      ),
      'entity_type' => array(
        'description' => 'Entity type.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
      ),
      'event' => array(
        'description' => 'Event to which should react.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
      ),
      'plugin' => array(
        'description' => 'Plugin type.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
      ),
      'config' => array(
        'type'        => 'text',
        'serialize'   => TRUE,
        'description' => 'Services client plugins configuration',
        'object default' => array(),
      ),
    ),
    'primary key' => array('eid'),
    'unique key' => array(
      'name' => array('name')
    ),
  );

  $schema['services_client_connection_id'] = array(
    'description' => 'Holds ids of remote clients',
    'export' => array(
      // Connection ID Name
      'key' => 'name',
      // Description of key.
      'key name' => 'Name of the connection',
      // For now, we don't allow to disable this.
      'can disable' => FALSE,
      // Variable name to use in exported code.
      'identifier' => 'connection_id',
      // Use the same hook as the API name below.
      'default hook' => 'default_services_client_connection_id_myobj',
      // CTools API implementation.
      'api' => array(
        'owner' => 'services_client',
        'api' => 'default_services_client_connection_id',
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'name' => array(
        'description' => 'The name of the connection.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
      ),
      'services_client_id' => array(
        'description' => 'ID of remote client.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
      ),
    ),
    'primary key' => array('name'),
  );

  return $schema;
}

/**
 * Implementation of hook_uninstall().
 */
function services_client_uninstall() {
  $variables = array('services_client_id', 'services_client_use_queue', 'services_client_process_queue_cron', 'services_client_exclude_users');
  foreach ($variables as $variable) {
    variable_del($variable);
  }
}

/**
 * Remove old {services_client_connection} table which has been moved to
 * services_client_connection module.
 */
function services_client_update_7001() {
  db_drop_table('services_client_connection');
}

/**
 * Add new {services_client_connection_id} which can store remote client id
 * with each connection.
 */
function services_client_update_7002() {
  $schema['services_client_connection_id'] = array(
    'description' => 'Holds ids of remote clients',
    'fields' => array(
      'name' => array(
        'description' => 'The name of the connection.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
      ),
      'services_client_id' => array(
        'description' => 'ID of remote client.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
      ),
    ),
    'primary key' => array('name'),
  );

  db_create_table('services_client_connection_id', $schema['services_client_connection_id']);
}

/**
 * Add ctools support and move data around
 */
function services_client_update_7003() {
  $schema = services_client_schema();
  // Services client verion 2 is installed, we can skip this step.
  if (!empty($schema['services_client_connection_event'])) {
    return;
  }

  ctools_include('export');
  if (db_table_exists('services_client_connection_hook') &&
        !db_field_exists('services_client_connection_hook', 'config')) {
    // Use what's already defined in uuid schema in order to be consistent.
    $schema = services_client_schema();
    $spec = $schema['services_client_connection_hook']['fields']['config'];
    db_add_field('services_client_connection_hook', 'config', $spec);
  }
  $hooks = ctools_export_crud_load_all('services_client_connection_hook');
  foreach($hooks as $hook) {
    // Copy over the settings from hook_mappings
    if (isset($hook->hook_mappings)) {
      $hook->config['mapping']['config'] = $hook->hook_mappings;
    }
    // Copy over the settings from hook_conditions
    if (isset($hook->hook_conditions)) {
      $hook->config['condition']['config'] = $hook->hook_conditions;
    }
    ctools_export_crud_save('services_client_connection_hook', $hook);
  }
  if (db_table_exists('services_client_connection_hook')) {
    // Use what's already defined in uuid schema in order to be consistent.
    db_drop_field('services_client_connection_hook', 'hook_mappings');
    db_drop_field('services_client_connection_hook', 'hook_conditions');
  }
}

/**
 * Create {services_client_connection_event} table.
 */
function services_client_update_7200() {
  // We're not going to change original services_client_connection_hook table so we
  // can run manual migration.
  $schema = services_client_schema();
  db_create_table('services_client_connection_event', $schema['services_client_connection_event']);
}
