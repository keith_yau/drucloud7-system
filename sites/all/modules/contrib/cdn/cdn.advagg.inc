<?php

/**
 * Implement hook_advagg_get_css_aggregate_contents_alter().
 */
function cdn_advagg_get_css_aggregate_contents_alter(&$data, $files, $aggregate_settings) {
  // Get base path escaped for regex.
  $base = str_replace('/', '\/', $aggregate_settings['variables']['base_path']);

  // Set context for cdn_file_url_alter().
  $conf_variables = array(
    CDN_MODE_VARIABLE => variable_get(CDN_MODE_VARIABLE, CDN_MODE_BASIC),
    CDN_BASIC_FARFUTURE_VARIABLE => variable_get(CDN_BASIC_FARFUTURE_VARIABLE, CDN_BASIC_FARFUTURE_DEFAULT),
    CDN_HTTPS_SUPPORT_VARIABLE => variable_get(CDN_HTTPS_SUPPORT_VARIABLE, FALSE),
  );
  foreach ($conf_variables as $name => $current_value) {
    $GLOBALS['conf'][$name] = $aggregate_settings['variables'][$name];
  }
  $old_https_server_value = NULL;
  if ($aggregate_settings['variables']['cdn_request_is_https'] && !cdn_request_is_https()) {
    if (isset($_SERVER['HTTPS'])) {
      $old_https_server_value = $_SERVER['HTTPS'];
    }
    else {
      $old_https_server_value = FALSE;
    }
    $_SERVER['HTTPS'] = 'on';
  }

  // Don't alter aggregation if this page is not going to use a CDN anyway.
  if (!cdn_check_protocol() && !$aggregate_settings['variables']['cdn_check_drupal_path']) {
    return;
  }

  // Find all paths in the CSS starting with the base path, ignoring external
  // and relative paths.
  $data = preg_replace_callback('/url\(\s*[\'"]?' . $base . '(?![a-z]+:)([^\'")]+)[\'"]?\s*\)/i', '_cdn_advagg_file_create_url', $data);

  // Set context back
  foreach ($conf_variables as $name => $current_value) {
    $GLOBALS['conf'][$name] = $current_value;
  }
  if (!is_null($old_https_server_value)) {
    $_SERVER['HTTPS'] = $old_https_server_value;
  }
}

/**
 * Similar idea to @see _drupal_build_css_path().
 *
 * Run preg_replace_callback $matches[1] through file_create_url(), allowing it
 * to be altered by hook_file_create_url_alter().
 *
 * @param $matches
 *   array of matched strings from preg_replace_callback().
 * @return
 *   url of file after being ran through file_create_url().
 */
function _cdn_advagg_file_create_url($matches) {
  if (empty($matches)) {
    return;
  }

  $return = 'url(' . file_create_url($matches[1]) . ')';
  return $return;
}
