<?php
/**
 * @file
 * drucloud_related_article.features.inc
 */

/**
 * Implements hook_views_api().
 */
function drucloud_related_article_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function drucloud_related_article_image_default_styles() {
  $styles = array();

  // Exported image style: article_related.
  $styles['article_related'] = array(
    'name' => 'article_related',
    'label' => 'article_related',
    'effects' => array(
      2 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 350,
          'height' => 234,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}
