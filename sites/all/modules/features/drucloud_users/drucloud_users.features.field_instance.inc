<?php
/**
 * @file
 * drucloud_users.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function drucloud_users_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'user-user-field_site'
  $field_instances['user-user-field_site'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 10,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_site',
    'label' => 'Site',
    'required' => 0,
    'settings' => array(
      'user_register_form' => 0,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Site');

  return $field_instances;
}
